Parse.Cloud.afterSave("SENSORS", function(request,response){  
    var Current = new Parse.Object.extend("CURRENT");
    r = request.object;
    var coreid = r.get("coreid");
    Parse.Analytics.track(coreid); // enables the tracking of core reporting frequency
    var o = new Current();
    o.set("Humid",      r.get("Humid"));
    o.set("rawTemp",    r.get("rawTemp"));
    o.set("Temp",       r.get("Temp"));
    o.set("rawLight",   r.get("rawLight"));
    o.set("Light",      r.get("Light"));
    o.set("rawMotion",  r.get("rawMotion"));
    o.set("Motions",    r.get("Motions"));
    o.set("Noises",     r.get("Noises"));
    o.set("Sound",      r.get("Sound"));
    o.set("Decibels",   r.get("Decibels"));
    o.set("SoundDev",   r.get("SoundDev"));
    o.set("Wifi",       r.get("Wifi"));
    o.set("coreid",     r.get("coreid"));
    o.set("Name",       r.get("Name"));
    o.save(null, {
        success: function(obj){
            var Current = new Parse.Object.extend("CURRENT");
            var findCore = new Parse.Query(Current);
            var id = obj.get("coreid");
            findCore.equalTo("coreid", id);
            findCore.descending("createdAt");
            findCore.find({
                success: function(items){//delete old instances of this device
                    for (var i = 0; i < items.length; i++) if (i>0) items[i].destroy({});  
                }
            });        
        }
    });
});
  
Parse.Cloud.define("read", function(request, response) {
    var Current = new Parse.Object.extend("CURRENT");
    var all = new Parse.Query(Current);
    all.find({
        success: function(results){
            response.success(results);
        }
    });
});

var _ = require('underscore');

//HOME OF FUTURE SUPER-QUERY (40K Stack)
//ULTIMATELY, this may not be required if normal use does not exceed the call count
// MAYBE, it returns a selected record first (as promise1, then returns the bigger promise right after)
Parse.Cloud.define("LiveHistory", function(request, response) {
    var Current = new Parse.Object.extend("CURRENT");
    var live = new Parse.Query(Current);
    var history = [];
    live.find().then(
        function(liveDevices){
            //Trivial resolved promise base case
            var promise = Parse.Promise.as();
            _.each(liveDevices, function(device) {
                //for each device, extend the promise with a function to push the historic data
                promise = promise.then(function() {
                    var LongTerm = new Parse.Object.extend("LONGTERM");
                    var did = device.get("coreid");
                    var lt = new Parse.Query(LongTerm);
                    lt.equalTo("coreid",did);
                    var time = new Date();
                    time.getTime();
                    time.setTime(time - (12*60*60*1000)); // last 12 hours as milliseconds
                    lt.greaterThanOrEqualTo("updatedAt", time);
                    lt.descending("createdAt");
                    return lt.find({
                        success: function(thisResult) {
                            for (var i=0;i<thisResult.length;i++){
                                var record = [];
                                //for each item, get these properties...
                                history.push(record);
                            }
                        },
                        error: function(error) {
                            console.log(error);
                        }
                    });
                });
            });
            return promise;
    }).then(function(result) {
            response.success(history);
    });
}); 
  
Parse.Cloud.job("longterm", function(request, response){
    var Current = new Parse.Object.extend("CURRENT");
    var all = new Parse.Query(Current);
    all.find().then(function(currents){
            var Sensors = new Parse.Object.extend("SENSORS");
            var time = new Date();
            time.getTime();
            time.setTime(time - (15 * 60000)); // last 15 minutes as milliseconds
            //FOR EACH CURRENT COREID, Create LONGTERM QUERY and WRITE
            var con = 0;
            for (var i = 0; i < currents.length; i++) {
                var tid = currents[i].get("coreid");
                var sense = new Parse.Query(Sensors);
                sense.equalTo("coreid", tid);
                sense.greaterThanOrEqualTo("updatedAt", time);
                sense.find().then(
                // create an array of promises 
                // do something with each result
                    function(items){ // all readings for the last 15 minutes
                        //action after promise return
                        var vals = [0,0,0,0,0,0,0,0,0,0];
                        var c = items.length;
                        if (c>0){ // only bother to archive if there are found records
                            for (var i = 0; i < c; i++){ // for each item
                                con++;
                                vals[0] += parseInt(items[i].get("Sound"));
                                vals[1] += parseInt(items[i].get("Light"));
                                vals[2] += parseInt(items[i].get("Wifi"));
                                vals[3] += parseInt(items[i].get("Motions"));
                                vals[4] += parseInt(items[i].get("Noises"));
                                vals[5] += parseFloat(items[i].get("Temp"));
                                vals[6] += parseInt(items[i].get("Humid"));
                                vals[7] += parseInt(items[i].get("SoundDev"));
                                vals[8] += parseInt(items[i].get("rawTemp"));
                                vals[9] += parseInt(items[i].get("rawLight"));
                                vals[10] += parseInt(items[i].get("rawMotion"));
                                vals[11] += parseInt(items[i].get("Decibels"));
                            }
                            // SAVE THE AVERAGE FOR THIS READING SET AS LONG-TERM
                            var longTerm = new Parse.Object.extend("LONGTERM"); 
                            var o = new longTerm();
                            o.set("coreid",     items[0].get("coreid"));
                            o.set("Sound",      String(parseInt(vals[0] / c)));
                            o.set("Light",      String(parseInt(vals[1] / c)));
                            o.set("Wifi",       String(parseInt(vals[2] / c)));
                            o.set("Motions",    String(parseInt(vals[3] / c)));
                            o.set("Noises",     String(parseInt(vals[4] / c)));
                            o.set("Temp",       String(parseFloat(vals[5]/c)));
                            o.set("Humid",      String(parseInt(vals[6] / c)));
                            o.set("SoundDev",   String(parseInt(vals[7] / c)));
                            o.set("rawTemp",    String(parseInt(vals[8] / c)));
                            o.set("rawLight",   String(parseInt(vals[9] / c)));
                            o.set("rawMotion",  String(parseFloat(vals[10]/c)));
                            o.set("Decibels",   String(parseInt(vals[11] / c)));
                            o.save(null);
                        }
                });
            }
    });
});
