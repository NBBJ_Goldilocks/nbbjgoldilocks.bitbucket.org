var services = angular.module('STL.services', []);

services.factory('cordovaReady', function($q) {
    var deferred = $q.defer();
    return {
        ready: function() {
                    document.addEventListener(
                        "deviceready",
                        function () {
                            deferred.resolve();
                        },
                        false
                    );
                    return deferred.promise;
                }
    }
});


// Services that can chare data between the "run" portion (login stuff) 
// and the "controller" portion (app stuff);
// Stack Overflow: http://stackoverflow.com/questions/18880737/how-do-i-use-rootscope-in-angular-to-store-variables
// JSFiddle: http://jsfiddle.net/gw8M9/

services.factory('allBundles', function() {
    var allBundles = undefined;
    var bundlesService = {};
    
    bundlesService.pull = function(user) {
      console.log('bundlesService: fired pull');
      var allBundles = [];
    
      // new parse query object
      var query = new Parse.Query('Bundle');

      // set query to current user
      query.equalTo('user', user);
      query.limit(1000);

      if (deviceSpecific && mobileOn) {
        console.log('device specific from mobile');
        query.equalTo('deviceUuid', device.uuid);
      }

      // set descending order by date
      query.descending('timestamp');

      return query.find({
        success: function(bundles) {
          console.log('bundlesService: retrieved bundles');
          console.log(bundles);
          var promise = new Parse.Promise();
          return promise; 
        }
      })
    };
    bundlesService.set = function(newBundles) {
        allBundles = newBundles;
        console.log('bundlesService: setBundles')
    };
    bundlesService.add = function(newBundle) {
        if (allBundles == undefined) {
          allBundles = [];
        }
        allBundles.unshift(newBundle);
    };
    bundlesService.get = function() {
        console.log('bundlesService: getBundles');
        return allBundles;
    };
    bundlesService.clear = function() {

        console.log('bundlesService: clearBundles');
        allBundles = undefined;
    };
    bundlesService.check = function() {
      // CHECK FOR NULL
      // console.log(allBundles);
      // console.log('bundlesService: allBundlesLength - ' + allBundles.length);
      if (!allBundles) {
        console.log('bundlesService: none found');
        return false;
      } else {
        console.log('bundlesService: bundles found');
        return true;
      }
    }
    
    return bundlesService;
});

services.factory('currentModel', function() {
    var currentModel = {};
    currentModel.categories = undefined;
    currentModel.searchString = undefined;
    var currentModelService = {};
    
    currentModelService.set = function(newCategories) {
        currentModel.categories = newCategories;
        console.log('currentModelService: setCurrentModel');
        console.log(currentModel);
    };
    currentModelService.setSearchString = function(newSearchString) {
        currentModel.searchString = newSearchString;
        console.log('currentModelService: setSearchString')
        console.log(newSearchString);
        console.log(currentModel);
    };
    currentModelService.get = function() {
        console.log('currentModelService: getCurrentModel');
        console.log(currentModel);
        return currentModel;
    };
    currentModelService.clear = function() {
        currentModel = {};
        currentModel.categories = undefined;
        currentModel.searchString = undefined;
        console.log('currentModelService: clearCurrentModel');
        console.log(currentModel);
    };
    currentModelService.check = function() {
      // CHECK FOR NULL
      // console.log(allcurrentModel);
      // console.log('currentModelService: allcurrentModelLength - ' + allcurrentModel.length);
      console.log(currentModel);
      if (!currentModel.categories) {
        console.log('currentModelService: none found');
        return false;
      } else {
        console.log('currentModelService: currentModel found');
        return true;
      }
    }
    
    return currentModelService;
});

services.factory('selectedBundles', function() {
    var selectedBundles = [];
    var selectedBundlesService = {};
    
    selectedBundlesService.set = function(newBundles) {
        for (var a in newBundles) {
          newBundles[a].myIndex = a;
          // console.log(newBundles[a].myIndex);
        }

        selectedBundles = newBundles;
        console.log('setBundles');
    };
    selectedBundlesService.add = function(newBundle) {
        selectedBundles.unshift(newBundle);
    };
    selectedBundlesService.get = function() {
        console.log('getBundles');
        return selectedBundles;
    };
    selectedBundlesService.clear = function() {
        selectedBundles = [];
    };
    
    return selectedBundlesService;
});

services.factory('selectedSearchBundles', function() {
    var selectedSearchBundles = [];
    var selectedSearchBundlesService = {};
    
    selectedSearchBundlesService.set = function(newBundles) {
        for (var a in newBundles) {
          newBundles[a].myIndex = a;
          // console.log(newBundles[a].myIndex);
        }

        selectedSearchBundles = newBundles;
        console.log('setSearchBundles');
    };
    selectedSearchBundlesService.add = function(newBundle) {
        selectedSearchBundles.unshift(newBundle);
    };
    selectedSearchBundlesService.get = function() {
        console.log('getSearchBundles');
        return selectedSearchBundles;
    };
    selectedSearchBundlesService.clear = function() {
        selectedSearchBundles = [];
    };
    
    return selectedSearchBundlesService;
});

services.factory('selectedBrowseBundles', function() {
    var selectedBrowseBundles = [];
    var selectedBrowseBundlesService = {};
    
    selectedBrowseBundlesService.set = function(newBundles) {
        for (var a in newBundles) {
          newBundles[a].myIndex = a;
          // console.log(newBundles[a].myIndex);
        }

        selectedBrowseBundles = newBundles;
        console.log('setBrowseBundles');
    };
    selectedBrowseBundlesService.add = function(newBundle) {
        selectedBrowseBundles.unshift(newBundle);
    };
    selectedBrowseBundlesService.get = function() {
        console.log('getBrowseBundles');
        return selectedBrowseBundles;
    };
    selectedBrowseBundlesService.clear = function() {
        selectedBrowseBundles = [];
    };
    
    return selectedBrowseBundlesService;
});

services.factory('userStats', function() {
    var userStats = {};
    var userStatsService = {};
    
    userStatsService.set = function(key, value) {
        userStats[key] = value;
    };
    userStatsService.get = function() {
        console.log('getUserStats');
        return userStats;
    };
    userStatsService.clear = function() {
        userStats = {};
    };
    
    return userStatsService;
});

services.factory('capturedMedia', function() {
    var capturedMedia = {};
    var capturedMediaService = {};
    
    capturedMediaService.set = function(mediaCollection) {
        capturedMedia = mediaCollection;
    };
    // capturedMediaService.add = function(mediaCollection) {
    //     capturedMedia = merge(capturedMedia, mediaCollection);
    // };
    capturedMediaService.get = function() {
        return capturedMedia;
    };
    capturedMediaService.clear = function() {
        capturedMedia = {};
    };
    
    return capturedMediaService;
});




services.factory('TagTemplates', [function() {
    
    console.log('set up existing tags');

      var templates = {
        name: "standard",
        selected: true,
        categories: [
          {
            name: "family",
            description: "These are your family members.",
            color: "red",
            checked: false,
            active: true,
            items: [
              {
                name: "Elizabeth", 
                checked: false,
                active: true
              },
              {
                name: "Charles", 
                checked: false,
                active: true
              },
              {
                name: "Harry", 
                checked: false,
                active: true
              },
              {
                name: "William", 
                checked: false,
                active: true
              },
              {
                name: "Diana", 
                checked: false,
                active: true
              }
            ]
          },
          {
            name: "friends",
            description: "Not your Facebook friends, your real life friends that appear in your photos.",
            color: "blue",
            checked: false,
            active: true,
            items: [
                {
                  name: "jay-z", 
                  checked: false,
                  active: true
                },
                {
                  name: "beyonce", 
                  checked: false,
                  active: true
                },
                {
                  name: "fred", 
                  checked: false,
                  active: true
                },
                {
                  name: "deleteThisFriend", 
                  checked: false, 
                  active: true
                }
              ]
          },
          {
            name: "setting",
            description: "A couple types of locations might be interesting.",
            color: "green",
            checked: false,
            active: true,
            items: [
                {
                  name: "home", 
                  checked: false,
                  active: true
                },
                {
                  name: "work", 
                  checked: false,
                  active: true
                },
                {
                  name: "city", 
                  checked: false,
                  active: true
                },
                {
                  name: "park", 
                  checked: false, 
                  active: true
                },
                {
                  name: "nature", 
                  checked: false, 
                  active: true
                }
              ]
          },
          {
            name: "event",
            description: "A couple types of locations might be interesting.",
            color: "purple",
            checked: false,
            active: true,
            items: [
                {
                  name: "holiday", 
                  checked: false,
                  active: true
                },
                {
                  name: "festival", 
                  checked: false,
                  active: true
                },
                {
                  name: "concert", 
                  checked: false,
                  active: true
                }
              ]
          },
          {
            name: "tap and hold me!",
            description: "A teaching moment.",
            color: "aqua",
            checked: false,
            active: true,
            items: []
          },
          {
            name: "archivedTag",
            description: "This is an archived tag.",
            color: "teal",
            checked: false,
            active: false,
            items: []
          }  
    ]};


    var catCounter = 0;
    var tagCounter = 0;

    for (var a in templates.categories) {
        var color = templates.categories[a].color
        templates.categories[a].myStyle = createStyle(color);
        templates.categories[a].myStyleUnchecked = createStyleUnchecked(color);
        templates.categories[a].selectedStyle = templates.categories[a].myStyleUnchecked;
        templates.categories[a].tagID = createID("Cat_", catCounter);
        catCounter++;

        for (var b in templates.categories[a].items) {
          templates.categories[a].items[b].parentID = templates.categories[a].tagID;
          templates.categories[a].items[b].tagID = createID("Tag_", tagCounter);
          templates.categories[a].items[b].selectedStyle = templates.categories[a].myStyleUnchecked;
          tagCounter++;
        }
    }

    console.log(templates);

    return {
        templates: templates
    }
}]);


// angular.module('starter.services', [])

// /**
//  * A simple example service that returns some data.
//  */
// .factory('PetService', function() {
//   // Might use a resource here that returns a JSON array

//   // Some fake testing data
//   var pets = [
//     { id: 0, title: 'Cats', description: 'Furry little creatures. Obsessed with plotting assassination, but never following through on it.' },
//     { id: 1, title: 'Dogs', description: 'Lovable. Loyal almost to a fault. Smarter than they let on.' },
//     { id: 2, title: 'Turtles', description: 'Everyone likes turtles.' },
//     { id: 3, title: 'Sharks', description: 'An advanced pet. Needs millions of gallons of salt water. Will happily eat you.' }
//   ];

//   return {
//     all: function() {
//       return pets;
//     },
//     get: function(petId) {
//       // Simple index lookup
//       return pets[petId];
//     }
//   }
// });


// FROM CONTROLLERS

var pullBundles = allBundles.pull($scope.currentUser);
Parse.Promise.when(pullBundles).then(function(bundles) {
  
}, function(error) {
});