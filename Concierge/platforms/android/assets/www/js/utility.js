function extractTime(string) {
    var startIndex = string.indexOf('T') + 1;
    var endIndex = startIndex + 8;
    return string.substring(startIndex, endIndex);
}

var Gradient = function (colors, width, center) {
    // this class constructs a simple 3-pt gradient and provides a method to compute a corresponding color from a value provided

    // the colors argument must contain 5 colors: below, bottom, center, top, above
    this.colors = colors;

    // we are building the gradient with the centerpoint because it will be used by the range sliders in the UI
    this.center = center;

    // the total width of the gradient
    this.width = width;
    
    // compute the lower and upper bounds of the gradient
    this.lowerBound = center - (width/2);
    this.upperBound = center + (width/2);
}

Gradient.prototype.getColor = function(values) {
    // define a function that will retrieve a color from two colors and a proportion
    function colorGenerator(color1, color2, proportion) {
        var newColor = {};
        function makeChannel(a, b) {
            return (a + Math.round((b - a) * proportion));
        }
        
        newColor.r = makeChannel(color1.r, color2.r);
        newColor.g = makeChannel(color1.g, color2.g);
        newColor.b = makeChannel(color1.b, color2.b);
        
        return (newColor);

        // SAMPLE USAGE
        // var yellow = { r: 255, g: 255, b: 0 };
        // var blue = { r: 0, g: 0, b: 255 };
        // var newColor = makeGradientColor(yellow, blue, 79);
    }
    function colorCSSAdder(justColor){
        function makeColorPiece(num) {
            num = Math.min(num, 255);   // not more than 255
            num = Math.max(num, 0);     // not less than 0
            var str = num.toString(16);
            if (str.length < 2) {
                str = "0" + str;
            }
            return (str);
        }
        justColor.cssColor = "#" +
                            makeColorPiece(justColor.r) +
                            makeColorPiece(justColor.g) +
                            makeColorPiece(justColor.b);
        return (justColor);
    }
    // create container for colors
    var newColors = [];
    for (var a in values) {
        // identify the correct colors and values to feed to the color generator
        var proportion, lowerColor, upperColor, color;
        if (values[a] < this.lowerBound) {
            color = this.colors[0];
        } else if ((values[a] >= this.lowerBound) && (values[a] < this.center)) {
            proportion = (values[a] - this.lowerBound) / (this.width / 2);
            color = colorGenerator(this.colors[1], this.colors[2], proportion);
        } else if ((values[a] >= this.center) && (values[a] <= this.upperBound)) {
            proportion = (values[a] - this.center) / (this.width / 2);
            color = colorGenerator(this.colors[2], this.colors[3], proportion);
        } else { // value is above the upper bounds
            color = this.colors[4];
        }
        newColors.push(colorCSSAdder(color));
    }
    return newColors;
}

// A NICE LITTLE FUNCTION THAT CALIBRATES THE BASELINES FOR CERTAIN SENSORS, LIKE "LOUD" SOUND AND LOW LIGHT
//THIS IS THE ONLY AREA OF Parse Object Dependence
function calibrateReadings(items){
    var baseLines = [
        {
        "Sound": "340",
        "coreid": "40003d000847343232363230"
    },{
         "Sound": "189",
        "coreid": "230046001447343433313338"
    },{
         "Sound": "300",
        "coreid": "370024000c47343432313031"
    },{
         "Sound": "189",
        "coreid": "3a001e001547343433313338"
    },{
        "Sound": "189",
        "coreid": "2d0021001347343339383037"
    },{
         "Sound": "189",
        "coreid": "3e0029000b47343138333038"
    },{
        "Sound": "189",
        "coreid": "1d0021001547343339383037"
    },{
        "Sound": "189",
        "coreid": "2e001c000a47343432313031"
    },{
         "Sound": "340",
        "coreid": "2d001f001347343339383037"
    },{
         "Sound": "540",
        "coreid": "27002e000947343337373738"
    },{
        "Sound": "340",
        "coreid": "3a003e000647343138333038"
    },{
         "Sound": "340",
        "coreid": "410034000447343138333038"
    },{
         "Sound": "540",
        "light": "0",
        "coreid": "1c0027000547343232363230"
    },{
         "Sound": "189",
        "coreid": "210022001147343339383037"
    },{
         "Sound": "189",
        "coreid": "360033000747343337373738"
    },{
        "Sound": "189",
        "coreid": "320027000447343233323032"
    },{
        "Sound": "189",
        "coreid": "26001f001347343339383037"
    },{
        "Sound": "189",
        "coreid": "230046001847343338333633"
    },{
        "Sound": "189",
        "coreid": "25003b001147343339383037"
    },{
        "Sound": "189",
        "coreid": "1b0032000b47343432313031"
    },
	{
        "Sound": "189",
        "coreid": "410031001047343339383037"
    },
	{
        "Sound": "189",
        "coreid": "210042001447343433313338"
    },{
        "Sound": "189",
        "coreid": "250042000347343337373739"
    },{
        "Sound": "189",
        "coreid": "25002e001147343339383037"
    },{
        "Sound": "340",
        "coreid": "36001e000f47343339383037"
    },{
        "Sound": "189",
        "light": "0",
        "coreid": "250044000747343232363230"
    },{
        "Sound": "189",
        "coreid": "1b002f001547343339383037"
    },{
        "Sound": "189",
        "coreid": "1f003b000247343138333038"
    },{
        "Sound": "189",
        "coreid": "260034000447343233323032"
    },{
        "Sound": "189",
        "coreid": "2e0036001047343432313031"
    },{
        "Sound": "300",
        "light": "0",
        "coreid": "1d0038001547343339383037"
    },{
        "Sound": "300",
        "coreid": "38003c001347343432313031"
    },{
        "Sound": "189",
        "coreid": "380024000f47343339383037"
    },{
        "Sound": "189",
        "coreid": "23001a001347343339383037"
    },{
        "Sound": "189",
        "coreid": "360029000747343232363230"
    },{
        "Sound": "189",
        "coreid": "400025000747343232363230"
    },{
        "Sound": "340",
        "coreid": "3b001f000547343233323032"
    },{
        "Sound": "340",
        "coreid": "290019000a47343432313031"
    },{
        "Sound": "189",
        "coreid": "220038001347343339383037"
    },{
        "Sound": "189",
        "coreid": "330042000347343339373536"
    },{
        "Sound": "189",
        "light": "0",
        "coreid": "370041000747343337373738"
    },{
        "Sound": "189",
        "coreid": "30002c001147343339383037"
    },{
        "Sound": "340",
        "coreid": "2b0026001347343339383037"
    },{
        "Sound": "189",
        "coreid": "300036000447343233323032"
    },{
        "Sound": "189",
        "coreid": "270047001447343339383037"
}];
    var fresh = [];
    count = items.length;
    
    for (var j=0;j<count;j++){
        var cor = items[j].get("coreid");
        
        //LIGHT
        //if 0 or light = 0, then return average light reading
        
        var i1 = items[j].get("Light");
        baseLines.forEach(function(each){
            if (each.coreid == cor){ 
                if(0 == each.light) i1 = 0; 
            }
        });
        //IF READING IS ZERO, ITS GOING TO STAY ZERO
        
        //i1 conversion to LUX
        var i1 =  Math.pow(parseFloat(i1),4.205576)/1029866117404.74+1;
        
        //SOUND
        var i2 = items[j].get("Sound");
            var commonBase; // THIS IS THE BASE READING THAT IS SUPPOSED BE BE 35 dB. Our reading range is most accurate at our inflection point, 60dB 
            commonBase = 191;
            var coreTest = "";
            baseLines.forEach(function(each){
                if (each.coreid == cor){ coreTest = each.Sound; }
            });
            if(coreTest != ""){commonBase = parseInt(coreTest);}
            
            i2 = 50*Math.log10(i2/commonBase) + 35;
        
        //TEMP - Here, will balance two readings and averaging
        var i3 = parseFloat(items[j].get("Temp")) -4.4;
        var i4 = parseFloat(items[j].get("Motions"));
        var i7 = parseFloat(items[j].get("rawMotion"));
        var i5 = parseFloat(items[j].get("Noises"));
        var i6 = items[j].get('updatedAt');
        var node = {"coreid": cor, "updatedAt":i6,"Sound":i2, "Light":i1, "Temp":i3, "Motions":i4, "Noises":i5, "rawMotion":i7};
        fresh.push(node);
    }
    return fresh; 
}