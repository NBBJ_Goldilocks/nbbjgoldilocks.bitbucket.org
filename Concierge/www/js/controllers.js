

angular.module('serg.controllers', [])

.controller('LoginCtrl', function ($scope, $state, $http) {
    //console.log($scope.currentUser);
    //var pollingInterval = 30000;
    ////var timezone = "America%2FNew_York";
    //var timezone = "America%2FLos_Angeles";
    //Chart.defaults.global.animation = false;
    //Chart.defaults.global.animationEasing = "easeInOutQuad";
    //Chart.defaults.global.animationSteps = 15;
    //Chart.defaults.global.tooltipTemplate = "<%= value %>";
    //Chart.defaults.global.tooltipCaretSize = 6;
    //$scope.options = {
    //    pointHitDetectionRadius: 6,
    //    bezierCurveTension: 0.5
    //}

    //Chart.defaults.global.scaleOverride = true;
    //Chart.defaults.global.scaleSteps = 12;
    //Chart.defaults.global.scaleStepWidth = 25;
    //Chart.defaults.global.scaleStartValue = 300;

    // Send a message to the user after the onboarding process
    $scope.firstAction = function () {
        $state.go("tab.map");
        if (mobileOn) {
            window.plugins.toast.show('1) play around with map\n2) search\n3) have fun', 'long', 'center', function (a) { console.log('toast success: ' + a) }, function (b) { console.log('toast error: ' + b) });
        }
    };


})

.controller('NavCtrl', function ($scope, $ionicSideMenuDelegate, $state, cordovaReady) {
    $scope.showMenu = function () {
        $ionicSideMenuDelegate.toggleLeft();
    };
   // $scope.showRightMenu = function () {
    //    $ionicSideMenuDelegate.toggleRight();
    //};
    $scope.sendFeedback = function () {
        
        // Make sure cordova is ready before firing 
        // (this will always be true by the time you push the feedback button)

        var promise = cordovaReady.ready();
        promise.then(function() {
            // window.plugin.email.isServiceAvailable(
            // function (isAvailable) {
            //     // alert('Service is not availnot available') unless isAvailable;
            //     if (isAvailable) {
            console.log(window);

            // SAMPLE EMAILCOMPOSER PLUGIN CALL //
            //window.plugins.emailComposer.showEmailComposerWithCallback(
            //    callback,
            //    subject,
            //    body,
            //    toRecipients,
            //    ccRecipients,
            //    bccRecipients,
            //    isHtml,
            //    attachments,
            //    attachmentsData);

            window.plugins.emailComposer.showEmailComposerWithCallback(
                function(value) {
                    console.log(value);
                }, 
                "I'd like to tell you a story about Goldilocks.", 
                "\n" + "\n" +
                "// device: " + device.model + "\n" +
                "// platform: " + device.platform + " " + device.version + "\n" +
                "// cordova: " + device.cordova + "\n" +
                "// appVersion: " + $scope.appVersion + "\n" + "\n",
                ['dc@nbbj.com'], 
                ['msyp@nbbj.com', 'danthony@nbbj.com','smckeever@nbbj.com', 'asoetarman@nbbj.com'],
                null, 
                false, 
                null, 
                null);
        });
            // } else {
        //   window.plugins.toast.show("Email not configured...", 'short', 'center', function(a){console.log('toast success: ' + a)}, function(b){console.log('toast error: ' + b)});  
        // }
        // }
        // );
    }
    $scope.logOut = function (form) {
        Parse.User.logOut();
        $scope.currentUser = null;
        $scope.scenario = 'Log in';
        $state.go("login");
    };
})

.controller('MapCtrl', function (ParseFactory, $interval, $state, $scope, $ionicScrollDelegate, initialResults, $ionicLoading, $ionicModal, $timeout) {
    
    // Take the raw data and store it as calibrated data
    var calibratedResults = calibrateReadings(initialResults);
    $scope.currentResults = calibratedResults;
                
    // Set initial search/browse state
    $scope.searchActive = false;
    $scope.numberSelected = 0;
    $scope.footerHidden = false;
    $scope.errorHidden = true;
    
    // DEFINE COLOR GRADIENTS FOR VARIOUS MODES
    var vlblue = { r: 202, g: 242, b: 250 };
    var lblue = { r: 113, g: 220, b: 255 };
    var mblue = { r: 0, g: 192, b: 255 };
    var lgreen = { r: 100, g: 250, b: 100 };
    var green = { r: 0, g: 255, b: 0 };
    var white = { r: 255, g: 255, b: 255 };
    var red = { r: 255, g: 0, b: 0 };
    var lgrey = { r: 220, g: 220, b: 220 };
    var grey = { r: 150, g: 150, b: 150 };
    var dgrey =    {r: 90,g: 90, b: 90 };
    var dseafoam = { r: 22, g: 88, b: 66 };
    var seafoam = { r: 43, g: 181, b: 131 };
    var lseafoam = { r: 55, g: 221, b: 168 };
    var vlseafoam = {r: 93,g: 250,b: 193};

    var lmagenta = { r: 255, g: 210, b: 255 };
    var magenta = { r: 235, g: 106, b: 185 };
    var dmagenta = { r: 156, g: 24, b: 114 };

    var lred = { r: 252, g: 199, b: 206 };
    var mred = { r: 255, g: 136, b: 150 };
    var dred = { r: 255, g: 51, b: 75 };

    //var lyellow = { r: 255, g: 252, b: 211 };
    //var myellow = { r: 255, g: 248, b: 132 };
    //var dyellow = { r: 255, g: 243, b: 46};
    var yellowGrey = { r: 191, g: 191, b: 191 };
    var lyellow = { r: 255, g: 248, b: 143 };
    var myellow = { r: 227, g: 220, b: 127 };
    var dyellow = { r: 191, g: 186, b: 107 };

    var lorange = { r: 255, g: 190, b: 74 };
    var morange = { r: 255, g: 146, b: 97 };
    var dorange = { r: 255, g: 90, b: 0 };

    var usualGradient = [white, white, seafoam, dseafoam, dseafoam];
    //var oldGradient = [white, white, lblue, mblue, mblue];
    //var searchGradientColors = [white, white, lgrey, vlseafoam, lseafoam];

    var sensorGradientColors = {
        "sound": [white, white, lmagenta, magenta, dmagenta],
        "temp": [white, white, lred, red, dred],
        //"light": [yellowGrey, yellowGrey, dyellow, myellow, lyellow], // dark to light
        // "light": [white, white, lyellow, myellow, dyellow], // light to dark
        // "light": [white, white, lyellow, myellow, dyellow], // light to dark
        "light": [lgrey, white, lorange, morange, dorange],
        //"light": [white, white, vlblue, lblue, mblue],
        "motion": [white, white, vlblue, lblue, mblue]
    }
    var searchGradientColors = [white, white, seafoam, dseafoam, dseafoam];

    // Put in Angular scope so the html template can access them
    // Note that number and order are important to the HTML template
    $scope.sensorRanges = [
      { "interfaceName": "noise", "units":"noises", "name": "Noises", "range": [0, 10], "searchRadius": 3},
      { "interfaceName": "sound","units":"decibels","name": "Sound", "range": [35, 70], "searchRadius": 5 },
      { "interfaceName": "temp",  "units":"F",      "name": "Temp", "range": [60, 80], "searchRadius": 3 },
      { "interfaceName": "light", "units":"lux",    "name": "Light", "range": [0, 500], "searchRadius": 80 },
      { "interfaceName": "motion","units":"Move Dif","name": "Motions", "range": [0, 2000], "searchRadius": 200 },
      { "interfaceName": "moves", "units":"motion", "name": "Motion", "range": [0, 10]  }
    ];              
    //OTHER POSSIBLE INCLUDE WI-FI, Humidity, "Feels like H+T"

    //Initialize slider start values
    $scope.searchValues = {
        "sound": 40,
        "temp": 70,
        "light": 250,
        "motion": 500
    }

    // Core IDs and locations
    var cores = [
        {"id": "370041000747343337373738","x":1421.148113,"y":157.697103,"name":"60"},
        {"id": "220038001347343339383037","x":1988.869783,"y":668.602757,"name":"59"},
        {"id": "300036000447343233323032","x":1400.910552,"y":279.122466,"name":"58"},
        {"id": "25002e001147343339383037","x":2137.096712,"y":856.09281,"name":"57"},
        {"id": "2d0021001347343339383037","x":1239.267438,"y":273.741803,"name":"56"},
      //{"id": "21001f001147343339383037","x":1355.609308,"y":1471.099494,"name":"55"},
        {"id": "38003c001347343432313031","x":1902.295991,"y":892.427683,"name":"54"},
      //{"id": "38003b000447343233323032","x":1093.57008,"y":1467.617567,"name":"53"},
        {"id": "1d0038001547343339383037","x":753.308616,"y":410.666609,"name":"52"},
        {"id": "360029000747343232363230","x":1224.486546,"y":155.494673,"name":"50"},
        {"id": "400025000747343232363230","x":607.376369,"y":191.467398,"name":"49"},
        {"id": "3e0029000b47343138333038","x":520.358056,"y":438.298276,"name":"48"},
        {"id": "36001e000f47343339383037","x":690.262903,"y":829.081006,"name":"47"},
        {"id": "420030000a47343432313031","x":1055.75614,"y":162.791948,"name":"46"}, // OFFLINE
        {"id": "2e001c000a47343432313031","x":2003.650674,"y":793.184555,"name":"45"},
        {"id": "210022001147343339383037","x":491.776648,"y":611.590748,"name":"44"},
      //{"id": "270041001047343339383037","x":834.237481,"y":1474.317839,"name":"43"},
        {"id": "270047001447343339383037","x":1066.183577,"y":285.314327,"name":"42"},
        {"id": "3b001f000547343233323032","x":629.027782,"y":609.479192,"name":"41"},
        {"id": "2d001f001347343339383037","x":805.250526,"y":181.723363,"name":"40"},
        {"id": "36001a000f47343339383037","x":189.824154,"y":862.8659,"name":"39"},
        {"id": "1b0032000b47343432313031","x":312.231938,"y":722.751106,"name":"38"},
        {"id": "1b003d001547343339383037","x":399.294473,"y":330.362079,"name":"37"}, // OFFLINE
        {"id": "26001f001347343339383037","x":2130.344028,"y":1133.145055,"name":"36"},
        {"id": "320031001247343339383037","x":1349.068345,"y":1084.57927,"name":"35"},//OFFLINE
        {"id": "380024000f47343339383037","x":594.746963,"y":1035.168925,"name":"34"},
        {"id": "230046001447343433313338","x":1135.801199,"y":1069.798378,"name":"33"},
        {"id": "360033000747343337373738","x":386.198853,"y":1014.897925,"name":"32"},
        {"id": "2b0026001347343339383037","x":392.533521,"y":911.431686,"name":"31"},
        {"id": "3a001e001547343433313338","x":869.745155,"y":1055.017487,"name":"30"},
        {"id": "330042000347343339373536","x":612.590087,"y":313.989778,"name":"29"},
        {"id": "1d0021001547343339383037","x":880.302934,"y":934.658801,"name":"28"},
        {"id": "390020000347343339373536","x":1357.863958,"y":970.028781,"name":"27"}, // OFFLINE
        {"id": "210042001447343433313338","x":197.770865,"y":722.751106,"name":"26"},
        {"id": "1b002f001547343339383037","x":179.266375,"y":225.176018,"name":"25"},
        {"id": "30002c001147343339383037","x":187.402606,"y":372.499409,"name":"24"},
        {"id": "27002e000947343337373738","x":174.637752,"y":1028.269181,"name":"23"},
      //{"id": "360033001047343339383037","x":1127.703485,"y":2424.303879,"name":"22"},
        {"id": "250044000747343232363230","x":1619.025737,"y":1110.013206,"name":"21"},
        {"id": "320027000447343233323032","x":206.894482,"y":521.973192,"name":"20"},
        {"id": "2a003a001047343339383037","x":1494.765702,"y":1088.802382,"name":"19"},//OFFLINE
        {"id": "260034000447343233323032","x":1834.726202,"y":1006.451701,"name":"18"},
        {"id": "27003b001347343339383037","x":1824.51921,"y":1110.030262,"name":"17"}, //OFFLINE
        {"id": "23001a001347343339383037","x":1632.016835,"y":979.001475,"name":"16"},
      //{"id": "2e002d001047343339383037","x":884.852759,"y":2140.978032,"name":"15"},
        {"id": "370024000c47343432313031","x":2124.009361,"y":651.71031,"name":"14"},
        {"id": "1c0027000547343232363230","x":495.99976,"y":805.853891,"name":"13"},
      //{"id": "3c0034000e47343432313031","x":674.132783,"y":1854.46791,"name":"12"},
      //{"id": "2e0041000c47343432313031","x":670.159518,"y":1484.954269,"name":"11"},
        {"id": "2e0036001047343432313031","x":322.350718,"y":518.516643,"name":"10"},
      //{"id": "230046001847343338333633","x":672.962458,"y":2200.141961,"name":"9"},
        {"id": "410031001047343339383037","x":823.366536,"y":302.462637,"name":"8"},
        {"id": "410034000447343138333038","x":373.838591,"y":212.613381,"name":"7"},
        {"id": "3a003e000647343138333038","x":1501.449759,"y":980.586561,"name":"6"},
        {"id": "290019000a47343432313031","x":1144.596812,"y":955.24789,"name":"5"},
        {"id": "25003b001147343339383037","x":352.413959,"y":810.077003,"name":"4"},
        {"id": "40003d000847343232363230","x":622.693114,"y":926.212577,"name":"3"},
        {"id": "1f003b000247343138333038","x":348.325056,"y":432.039055,"name":"2"},
        {"id": "250042000347343337373739","x":2136.678696,"y":1017.009481,"name":"1"}  
    ];

    // BUTTON STATES AND FUNCTIONS TO CONTROL FOOTER AND MAP VIZ

    // Set variable element heights
    var browseHeight = 200;
    var searchHeight = 260;
    var buttonHeight = 44;
    var navHeight =     20;
    var sliderHeight = 45;

    // Set initial footer height based on mode
    $scope.footerHeight = browseHeight + "px";
    $scope.chartHeight = (browseHeight - buttonHeight) + "px";
    // Set up spinners
    
    $scope.showSpinner = function () {
        $ionicLoading.show({
            template: '<ion-spinner icon="lines"></ion-spinner>',
            noBackdrop: true
            //, duration: 8000
            //template: '<div><p>Loading... </p></div>'
        });
        //console.log('showSpinner()');
    };

    $scope.hideSpinner = function () {
        $ionicLoading.hide();
        //console.log('hideSpinner()');
    };

    // Funtion to toggle browse/search mode
    $scope.toggleSearchActive = function () {
        $scope.searchActive = !$scope.searchActive;
        if ($scope.searchActive) {
            $scope.svgHeight = (iY - browseHeight );  // Trigger browse based on incoming type
            setFooterSvgHeight();
        } else {
            $scope.svgHeight = (iY - browseHeight) + "px";
            $scope.footerHeight = browseHeight + "px";
        }
        $scope.showSpinner();
        // Required timeout to avoid race condition with spinner
        $timeout(function () {
            drawColorGrid();
        }, 20)
        
        // $scope.currentResults = $scope.searchActive ? $scope.calibratedSearchResults : $scope.calibratedBrowseResults;
        // Toggle D3 viz based on search (probably restore last search state if it exists.)
    };

    
    // Is Search enabled? Called from Angular template to control which controls are showing.
    $scope.isSearchActive = function () {
        return $scope.searchActive;
    };
 
    // Are no categories selected?? Need to determine whether to display help message.
    $scope.areCategoriesBlank = function () {
        return $scope.numberSelected == 0;
    }

    // BROWSE CONTROL
    // Set initial browse category
    $scope.active = "sound";

    // Is this category active? Called from Angular template to control if button appears selected.
    $scope.isActive = function (type) {
        return type === $scope.active;
    };

    $scope.setActive = function (type) {
        $scope.showSpinner();
        // The following timeout is necessary to avoid a race condition between the showSpinner() and drawColorGrid, which then calls hideSpinner()
        // Without the timeout, showSpinner is blocked until after drawColorGrid completes, meaning the spinner never appears
        $timeout(function () {
            $scope.active = type;
            d3.select(".ion-stats-bars").style("display", "none");
            drawColorGrid();
        }, 20)
        
    };

    $scope.searchUpdate = function () {
        console.log('updating search');
        $scope.showSpinner();
        $timeout(function () {
            drawColorGrid();
        }, 20)
        
    };

    $scope.openFavorites = function () {
        $scope.openModal();
        
    };

    $ionicModal.fromTemplateUrl('faves-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.modal = modal;
        
    });
    $scope.openModal = function () {
        $scope.modal.show();
        
    };
    $scope.closeModal = function () {
        $scope.modal.hide();
        
    };
    //Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function () {
        $scope.modal.remove();
        
    });
    // Execute action on hide modal
    $scope.$on('modal.hidden', function () {
        
    });
    // Execute action on remove modal
    $scope.$on('modal.removed', function () {
        
    });
    
    // Slider Presets for the FAVES modal

    $scope.presets = {
        coolQuiet: {sound: 40, temp: 68, light: null, motion: null},
        loudLively: { sound: 65, temp: null, light: null, motion: 700 },
        warmBright: { sound: null, temp: 75, light: 550, motion: null },
    };

    $scope.adjustSliders = function (values) {
        console.log("alright, let's change some values", values);
        for (var a in values) {
            console.log(a, values[a]);
            if (values[a] == null) {
                $scope.searchCategories[a] = false;  // deactivate the slider
            } else {
                $scope.searchCategories[a] = true; // activate the slider
                //console.log($scope.searchCategories);
                $scope.searchValues[a] = values[a]; // set slider to preset value
            }


        }
        $scope.numberSelected = 0;

        // Count number of active categories
        for (var a in $scope.searchCategories) {
            if ($scope.searchCategories[a]) {
                $scope.numberSelected++;
            }
        }
        // Set footerHeight dynamically
        setFooterSvgHeight();
        $scope.showSpinner();
        // Required timeout to avoid race condition with spinner
        $timeout(function () {
            drawColorGrid();
        }, 20)
        $scope.modal.hide();
        
    };

    // SEARCH CONTROL
    // Create container hold search category active toggles
    $scope.searchCategories = { "sound": false, "temp": false, "light": false, "motion": false }; // for plugging in to Parse search 
    $scope.numberSelected = 0;
    
    // Is this category active? Called from Angular template to control if button appears selected.
    $scope.isSearching = function (type) {
        return $scope.searchCategories[type];
    };

    function setFooterSvgHeight() {
        var footerHeight;
        if ($scope.searchActive) {
            console.log($scope.numberSelected);
           if ($scope.numberSelected != 0) {
                footerHeight = navHeight + ($scope.numberSelected * sliderHeight) + 44;
            } else {
                footerHeight = navHeight + sliderHeight + 44;
            }
            $scope.svgHeight = (iY - footerHeight - 44) + "px";// Trigger browse viz based on incoming type
            $scope.footerHeight = footerHeight;
        } else {
            if ($scope.footerHidden) {    // In Browse Mode: Check if footer has been hidden
                footerHeight = navHeight + sliderHeight + 40;
                $scope.chartHeight = buttonHeight + "px";
            } else {
                footerHeight = browseHeight;
                $scope.chartHeight = (browseHeight - buttonHeight) + "px";
            }
            $scope.svgHeight = (iY - footerHeight -44) + "px";// Trigger browse viz based on incoming type  
        }
        $scope.footerHeight = footerHeight + "px";
        
    }

    $scope.toggleFooter = function () {
        //console.log('test toggleFooter');
        $scope.footerHidden = !$scope.footerHidden;
        setFooterSvgHeight();
    }

    $scope.setSearchActive = function (type) {
        $scope.searchCategories[type] = !$scope.searchCategories[type];
        $scope.numberSelected = 0;

        // Count number of active categories
        for (var a in $scope.searchCategories) {
            if ($scope.searchCategories[a]) {
                $scope.numberSelected++;
            }
        }
        
        // Set footerHeight dynamically

        setFooterSvgHeight();
        $scope.showSpinner();
        // Required timeout to avoid race condition with spinner
        $timeout(function () {
            drawColorGrid();
        }, 20)
        
    };

    // GLOBAL VARIABLES
    // Let's set some global controller variables here that allow us to change look and feel really quickly based on changing UI needs
    // Remember that "result" is the global container for query returns (promised on startup, updated periodically)

    //D3 universal variables for graphing
     var margin = 10,
        iX = innerWidth, iY= innerHeight,
        //these are for 3d Graph sizes
        height = browseHeight - margin*2 - 74, //minus the buttons at base + buffers
        width = iX-margin*2;
    
    setFooterSvgHeight();

    // SVG ZOOM CONTROL
    // min-max zoom of plan
    var minZoom = 0.09/320*iX,
        startZoom = .127/320*iX,
        maxZoom = 1.8;

    $scope.historical = null; // this is the marker that will tell us if we are looking at the historic graphs
    var pollingInterval = 30000; 

    // UTILITY FUNCTIONS
    // Let's also create some utility functions (maybe can be broken out into a separate js that is called from index.html)
    function returnSensorRange(name) {
        var r = ""
        $scope.sensorRanges.forEach(function (f) {
            var interfaceName = f["interfaceName"];
            if (interfaceName == name) r = f;
        });
        return r;
    } // return the set of attributes for the requested sensor 

    function returnActiveSensorRange() {
        var r = "",
            name = $scope.active;
        $scope.sensorRanges.forEach(function (f) {
            var interfaceName = f["interfaceName"];
            if (interfaceName == name) r = f;
    });
        return r;
    } // return the set of attributes for the active sensor in the scope (from UI)

    // D3 CHARTS

    function histogram(read, featureGradient) {
        $scope.errorHidden = true;
        d3.selectAll(".activeTip").classed("activeTip",false);
        d3.selectAll(".activeCircle").classed("activeCircle",false);
        var formatCount = d3.format(",.0f");
        var past = d3.selectAll(".charted");
        if (past!=null) past.remove();
        
        var activeSensorRange = returnActiveSensorRange();
        var range = activeSensorRange['range'];
        
        var x = d3.scale.linear()
            .domain([range[0], range[1]])
            .range([0, width-margin*4]);
       
        var binCount = 12;
        var data = d3.layout.histogram()
            .bins(x.ticks(binCount))
            (read);

        var y = d3.scale.linear()
            .domain([0, d3.max(data, function(d) { return d.y; })])
            .range([(height/2), 0]);

        var xAxis = d3.svg.axis().orient("bottom").scale(x);
        var svg = d3.selectAll(".bar").select("svg");
        svg.attr("height", height);

        var char = svg.append("g").attr("class", "charted")
            .attr("width", width-margin*2)
            .attr("height", height)
            // .attr("transform", "translate(" + margin + "," + margin + ")"); 
            .attr("transform", "translate(" + "0," + margin + ")"); //

        var bucket = char.selectAll(".bucket")
            .data(data)
            .enter().append("g")
            .attr("class", "bucket")
            .attr("transform", function(d) { return "translate(" + (x(d.x)+margin*2) + "," + ( y(d.y)) + ")"; }); 
        
        bucket.append("rect")
            .attr("x", 1)
            .attr("transform", "translate("+ 0 +","+ (height/2 ) + ")")
            .attr("width", x(data[0].dx + range[0]) - 3)
            .attr("height", function(d) { return (height/2-y(d.y)); })
            .style("fill", function(d) { 
                var col = featureGradient.getColor([d.x]);
                col = col[0].cssColor;
                return col;
            });           
        bucket.append("text")
            .attr("transform", "translate("+ x(data[0].dx + range[0])/2 +","+ height/3 + ")")
            .attr("text-anchor", "left")
            .text(function(d) { return formatCount(d.y); });

        char.append("g")
            .attr("class", "x axis")
            .attr("font-size", 64)
            .attr("transform", "translate("+ margin*2 +","+ (height) + ")")
            .call(xAxis);
    } 
    
    var historicByID = []; 
    function timeLineHistoryCheck(id){
        var coreTest = historicByID[id];
        $scope.errorHidden = true;
        if(typeof coreTest !== "undefined"){
            data = coreTest;
            timeLine(data);
            $scope.footerHidden = false;
            setFooterSvgHeight();
            if ($scope.searchActive) { $scope.toggleSearchActive(); }
       //     console.log('timeline found in cache');
            if(!data.length) $scope.errorHidden = false;
        } else {
            ParseFactory.getLT(id,function(data) {
                data = calibrateReadings(data);
                historicByID[id] = data;
                timeLine(data);
                $scope.footerHidden = false;
                setFooterSvgHeight();
                if ($scope.searchActive) { $scope.toggleSearchActive(); }
        //        console.log('timeline retrieved');
                if(!data.length) $scope.errorHidden = false;
                $scope.$apply(); 
            });
        }
        
    }
                                
    function timeLine(data) {
        //if ID exists, do, else query
            $scope.historical = data; 
            var activeSensorRange = returnActiveSensorRange();
            var featureGradient = new Gradient(sensorGradientColors[$scope.active], (activeSensorRange.range[1]-activeSensorRange.range[0]), ((activeSensorRange.range[1]+activeSensorRange.range[0])/2));

            var line = d3.svg.line().interpolate("linear")
                .x(function(d) { return x(d.date); })
                .y(function(d) { return y(d.v);    });
                
            var arrayed = [];
            $scope.historical.forEach(function(d){
                arrayed.push({
                    "date": new Date(d['updatedAt']),
                    "v": d[activeSensorRange['name']] 
                });
            }); 
            
            //HERE'S WHERE WE LOCK THE HISTORY TIME SCALE TO THE LAST 24 HOURS POINTS
            var d = new Date();
            var yester = d3.time.hour.offset(d, -12);
            var x = d3.time.scale()
                .domain([yester, d])
                .range([0, width-margin*4]);
 
            var y = d3.scale.linear()
                .domain([activeSensorRange.range[0], activeSensorRange.range[1]])
                .range([(height - 2*margin), 0]);
      
            var xAxis = d3.svg.axis()
                .scale(x)
                .ticks(d3.time.hours, 2)
                .tickFormat(d3.time.format("%H %p"))
                .orient("bottom");
                    
            var yAxis = d3.svg.axis()
                .scale(y)
                .orient("left").ticks(5);    
            
            var svg = d3.selectAll(".bar").select("svg");
            var past = svg.selectAll(".charted");
            past.remove();
        
            var historyChart = svg.append("g").attr("class", "charted")
                .attr("width", width)
                .attr("height", height)
                .attr("transform", "translate(" + margin + "," + margin + ")");     
            
            historyChart.append("g")
                .attr("class", "x axis")
                .attr("transform", "translate("+margin*2 +"," + (height) + ")")
                .call(xAxis);

            historyChart.append("g")
                .attr("class", "y axis")
                .attr("transform", "translate(" + margin*2 + "," + 16 + ")") 
                .call(yAxis)
                .append("text")
                .attr("transform", "rotate(-90)")
                .attr("y", 6)
                .attr("x", 0)
                
                .attr("font-size", 64)
                .attr("dy", "10")
                .style("text-anchor", "end")
                .text(activeSensorRange['units']);
            
            var ticks = d3.selectAll(".y.axis").selectAll('g.tick')
                .append("circle")
                .attr("x",-10)
                .attr("y",10)
                .attr("r", 4)
                .style("fill", function(d) { 
                    var col = featureGradient.getColor([parseInt(d)]);
                    col = col[0].cssColor;
                    return col;
                });
            // Pass in Color for Line
            historyChart.append("path")
                .datum(arrayed)
                .attr("class", "line")
                .attr("transform", "translate("+margin*2 +"," + 16 + ")")
                .attr("d", line);
            
        // DATA POINTS PARAMETERS
            var dataPointRadius = 3;

            var formatTime = d3.time.format("%e %B");
            // HERE'S DATA POINT SHAPES AND HOVER VALUES AND CLICK ACTION
            var charted = d3.select(".charted");
            var points = charted.selectAll(".data-point")
                .data(arrayed)
                .enter().append("g");
            var tt = 0;
            points.append('circle')
                .attr("r", dataPointRadius)
                .attr("stroke", "black")
                .style("fill", function(d) { 
                    var col = featureGradient.getColor([parseInt(d.v)]);
                    col = col[0].cssColor;
                    return col;
                }) 
                .attr('class', 'data-point')
                .attr('cx', function(d) { return x(d.date)})
				.attr('cy', function(d) { return y(d.v)})
                .attr("transform", "translate(" + margin*2 + "," + 16 + ")");
            points.append("text")
                    .attr("class", "thooltip").html(function(d) {
                        var ctnt = "";
                        tt++;
                        tt = tt%4;
                        if (tt==0) ctnt = parseInt(d.v);
                        return ctnt;
                    })
                    .attr('x', function(d) { return (x(d.date))})
				    .attr('y', function(d) { return (y(d.v)-10)})
                    .style("opacity", 1)
                .attr("transform", "translate(" + margin*2 + "," + 10 + ")");
            d3.select(".ion-stats-bars").style("display","block").on("click",function(){
                reHist(); 
                d3.event.preventDefault(); // this is important for click handling
                d3.event.stopPropagation(); // prevents event propagation down the chain to the Zoom controller, causing havoc
                d3.select(this).style("display","none");
            });
        }    

    // DRAW HELPER FUNCTIONS
    
    function reHist() {
        $scope.historical = null;
        var  activeSensorRange = returnActiveSensorRange();  
        var featureGradient = new Gradient(sensorGradientColors[$scope.active], (activeSensorRange.range[1] - activeSensorRange.range[0]), ((activeSensorRange.range[1] + activeSensorRange.range[0]) / 2));

        var read = [];
        for (var j = 0; j < $scope.currentResults.length; j++) { // consider each sensor reading and make a tooltip
            read[j] = $scope.currentResults[j][activeSensorRange['name']];
        }
        histogram(read, featureGradient);
    }  // redraws all the colors and tooltips but not the entirety of the SVG 

    function sensorToolTip(activeSensorRange, read, cor) {
        activeSensorRange = returnActiveSensorRange();  
        //TEXT IN TOOLTIP
        d3.select("[id='ttRead" + cor + "']").text(parseInt(read));
        d3.select("[id='ttUnits" + cor + "']").text(activeSensorRange['units']); 
    }
    
    // GIVES A COLOR SCORE FOR EACH GRID-UNIT, REALLY IMPT FOR SEARCH
    function CalculateValue(i) { // passed the location of the svg for calculation
        var read, myValue;
        var calculatedValue = 0;
        var out = 0, w = 0; // variables: sum of readings divided by distance, and sum of all distances
        // Check to see if search is enabled
        if ($scope.searchActive) {
            if ($scope.numberSelected == 0) {
                // Map white to all squares... 
                calculatedValue = 1;
            } else { // Calculate search values based on multiple slider targets
                var selectedCategories = [];
                var selectedRanges = [];
                var selectedSearchValues = [];
                var myScores = [];

                // FIRST LET'S DETERMINE THE SELECTED CATEGORIES AND GET THEIR RANGES
                for (var a in $scope.searchCategories) { // where 'a' is the key -- name of the category -- and the value is a boolean
                    if ($scope.searchCategories[a]) {  // this category is selected in the UI                  
                        // get the relevant range object and slider value
                        var selectedRange = returnSensorRange(a);
                        var selectedSearchValue = parseInt($scope.searchValues[a]);
                        selectedRanges.push(returnSensorRange(a));
                        selectedSearchValues.push(parseInt($scope.searchValues[a]));
                        myValue = 0;
                        // For the current cores, calculate the current value of that category
                        for (var j = 0; j < $scope.currentResults.length; j++) { // iterate through each Core to extract values
                            read = parseInt($scope.currentResults[j][selectedRange['name']]); // get the value of the active sensor for this Core
                            if (read > selectedRange.range[1]) read = selectedRange.range[1];
                            if (read < selectedRange.range[0]) read = selectedRange.range[0];
                            myValue += read*distanceSVGcores[i][j];
                        }
                        // Let's compare myValue to the search target and give it a score based on searchRadius
                        var myScore = Math.abs(myValue - selectedSearchValue) / selectedRange.searchRadius;
                        myScores.push(myScore);
                    }                 
                }
                for (var a in myScores) {
                    calculatedValue += Math.abs(parseFloat(myScores[a]));
                }
                calculatedValue = calculatedValue / myScores.length;
                //Limits from 0->1 to 1->0, 0 min
                if (calculatedValue > 1) calculatedValue = 0; else calculatedValue = 1 - calculatedValue;
        }

        } else { // we are in browse mode, proceed as usual
            var activeSensorRange = returnActiveSensorRange();
            var distances = distanceSVGcores[i];
            for (var j = 0; j < $scope.currentResults.length; j++) { // iterate through each Result to extract values
                read = parseInt($scope.currentResults[j][activeSensorRange['name']]); 
                if (read > activeSensorRange.range[1]) read = activeSensorRange.range[1];
                if (read < activeSensorRange.range[0]) read = activeSensorRange.range[0];
                calculatedValue += read*distanceSVGcores[i][j];
            }
        }
        return calculatedValue;
    }

    // PRIMARY DRAW FLOORPLATE FUNCTIONS + CREATES ZOOMING
    var distanceSVGcores, closestAverage;
    function calculateDistances() {
        distanceSVGcores = [];
        closestAverage =   [];
        var lowestD = [];
        var svgs = d3.select("#BG").selectAll("g"); // the BG layer is the one we set out to colors
            svgs = svgs[0];
        //Going to create an array for each ColorGrid
        var locs = [];
        for (var j=0;j<calibratedResults.length;j++){
            var loc = 0;
            var cor = calibratedResults[j]["coreid"];
            var thisCirc = d3.select("#c"+cor);
            if(thisCirc[0][0] != null){
                loc = { "x": thisCirc.attr("cx"), "y": thisCirc.attr("cy") };
            }
            locs.push(loc);
        }
        for (var n=0;n<cores.length;n++){
            lowestD[n]=10000000000;
            closestAverage[n]=0;
        }
        
        for (var i = 0; i < svgs.length; i++) { 
            var bbox = svgs[i].getBBox();
            var cen = { "x": bbox.x + bbox.width /2, "y": bbox.y + bbox.height /2};
            for (var n=0;n<cores.length;n++){
                 var distanceSquared = Math.pow(cen.x - cores[n].x, 2) + Math.pow(cen.y - cores[n].y, 2) + 0.00000000000000000001;
                if (distanceSquared < lowestD[n]){ lowestD[n] = distanceSquared; closestAverage[n] = i;} 
            }
            var theseDistances = [], sumDistance = 0;
            // that is in turn a set of score weights out of 1
            for (var k=0;k<locs.length;k++){
                  if (locs[k] != 0){
                    var distanceSquared = Math.pow(cen.x - locs[k].x, 2) + Math.pow(cen.y - locs[k].y, 2) + 0.00000000000000000001;
                    sumDistance += 1/distanceSquared;
                    theseDistances.push(1/distanceSquared);
                  } else theseDistances.push(0);
            }
            // HOW ABOUT THE OTHER GROUP? 
            var testSum = 0;
            for (var k=0;k<locs.length;k++){
                 theseDistances[k] = theseDistances[k] / sumDistance;
                 testSum += theseDistances[k];
            }
            distanceSVGcores.push(theseDistances);
        }
    }
    
    function loadNetwork() {// load initial sensor locations as circles for reference (in a particular layer) and attached zoom actions
        var startPoint = [15/320*iX,86/320*iX];
        d3.select("svg").append("g").attr("id", "tips");
        var tips = [];
        var circles = d3.select("svg").append("g").attr("id", "circ");
        d3.select("svg").append("g").attr("id", "counts");
        for (var c = 0; c < cores.length; c++) {  // draw all the circles and tool tips for the given sensor locations.  These objects will henceforth relate to data by ID
            var loc = { "x": cores[c]["x"], "y": cores[c]["y"] };
            circles.append("circle").attr("id", "c"+ cores[c]["id"]).attr("cx", loc.x).attr("cy", loc.y).attr("r", 8);
            tips[c] = d3.select("svg").select("g#tips").append("g").attr("class", "over").attr("id", cores[c]["id"]).attr("cursor", "pointer");
            tips[c].append('rect').attr("width", 50).attr("height", 40)
                .attr("x", loc.x).attr("y", loc.y -40 -15);
            tips[c].append('text').attr("class","ttRead").attr("id", "ttRead"+cores[c]["id"]).attr("x", loc.x + 3).attr("y", loc.y -18 -15);
            tips[c].append('text').attr("class","ttUnits").attr("id", "ttUnits"+cores[c]["id"]).attr("x", loc.x+ 3).attr("y", loc.y -6 -15);
            tips[c].append('path').attr('d', function(d) { 
                var x = loc.x, y = loc.y;
                return 'M ' + x +' '+ y + ' l 0 -55';
            });
             tips[c].on("touchstart", function () { 
                d3.selectAll(".activeTip").classed("activeTip",false);
                d3.selectAll(".activeCircle").classed("activeCircle",false);
                var div = d3.select(this).classed("activeTip", true);
                var circ = d3.select("g#circ");
                cid = "[id='c" + this.id + "']";
                var it = circ.select(cid);
                //Highlight selected circle.
                it.classed("activeCircle", true);
                console.log(this.id); // and possibly the name, too
                // very important, binds historic charting to these elements
                timeLineHistoryCheck(this.id);
                d3.event.preventDefault(); // this is important for click handling
                d3.event.stopPropagation(); // prevents event propagation down the chain to the Zoom controller, causing havoc
            });
        }
        d3.selectAll("g.over").style("visibility", "hidden"); 
        var movable = d3.select("#svgContainer").selectAll("svg > g").attr("transform", "translate("+startPoint+") scale("+startZoom+")"); // specifies the parts that get adjusted

        var x = d3.scale.linear()
                .domain([-10, 0, 100])
                .range([0, width/2, width]);

        var y = d3.scale.log()
                .domain([1,height])
                .range([height, 0]);
        
        var zm = d3.behavior.zoom()
            .translate(startPoint).scale(startZoom)
            .scaleExtent([minZoom, maxZoom])
            .on("zoom", zoomAction);
            
        // for a zoom event, scales and translates, if the event is a drag or wheel, based on svg size
        var panExtent = {x: [-2200,2200], y: [-1500,1500] };
        var svg = d3.select("svg#map");
        function zoomAction() { 
            //limit the extent of pan but otherwise ok
             var t = d3.event.translate;
             var z = d3.event.scale;
             var x = t[0];
             var y = t[1];
             var xa = (panExtent.x[1]-0.5*iX)*z, xi = (panExtent.x[0]+0.5*iX/z)*z, ya = (panExtent.y[1]-0.5*iY)*z, yi = (panExtent.y[0]+0.5*iY/z)*z;
             var x = Math.min(x, xa);
                 x = Math.max(x, xi);
             var y = Math.min(y, ya);
                 y = Math.max(y, yi);
             zm.translate([x,y]);
             // HIDE/SHOW TOOLTIPS
             if(z<0.4) d3.selectAll("g.over").style("visibility", "hidden"); 
             else d3.selectAll("g.over").style("visibility", "visible");
             movable.attr("transform", "translate(" +x+","+y+ ")" + " scale(" + d3.event.scale + ")"); 
        }
        svg.call(zm);
        svg.on("mousemove.zoom", function () {
              d3.event.preventDefault();  
            d3.event.stopPropagation();
        });
        svg.on("touchstart", function () {
            d3.event.preventDefault();  
            d3.event.stopPropagation();
        });
        svg.on("dblclick", function () {
            d3.event.preventDefault();  
            d3.event.stopPropagation();
        });
        
        svg.style("width", window.innerWidth).style("pointer-events", "all");
    } // one-time load operation, setup of the SVG background.
    
    //Re-adjusts click stopping when the containers change
    function stopProp(){
           var footerStop = d3.selectAll(".searchSlider")
            .on(".zoom", null)
            .on("mousemove.zoom", function () {
                d3.event.preventDefault();  
                d3.event.stopPropagation();
            })
            .on("touchstart.zoom", function() {
                d3.event.preventDefault(); 
                d3.event.stopPropagation();
            })
            .on("dblclick.zoom", function () {
                d3.event.preventDefault();  
                d3.event.stopPropagation();
            });
            console.log("slide clear");
        // DO NOT INCLUDE THE SLIDER OBJECT IN THESE TOUCH CONTROLS
    }
    
    function drawColorGrid() {
       // get active sensor, active gradient, active slider value
        var read = [];
        var activeSensorRange = returnActiveSensorRange();     
        var activeGradientColors = sensorGradientColors[$scope.active];
        $scope.historical = null; 
        var svgs = d3.select("#BG").selectAll("g"); // the BG layer is the one we set out to colors
        svgs = svgs[0];
        var featureGradient = new Gradient(sensorGradientColors[$scope.active], (activeSensorRange.range[1] - activeSensorRange.range[0]), ((activeSensorRange.range[1] + activeSensorRange.range[0]) / 2));
        var searchGradient = new Gradient(searchGradientColors, 1, 0.5);
        
        //AREAS to color, get the right value + repaint
        for (var i = 0; i < svgs.length; i++) { 
            var calculatedValue = CalculateValue(i); // get all calculated
            var myColor;
            if ($scope.searchActive) {
                if ($scope.numberSelected == 0) {
                    myColor = searchGradient.getColor([0.5]);
                } else {
                    myColor = searchGradient.getColor([calculatedValue]);
                }
            } else {
                myColor = featureGradient.getColor([calculatedValue]);
            }
            svgs[i].style.fill = myColor[0].cssColor;
        }
        
        //DEFAULTS for all Sensors
        for (var s = 0; s < cores.length; s++) {
            var calculatedValue = CalculateValue(closestAverage[s]); // get all calculated
            var cor = cores[s]["id"];
            //WRITE TOOLTIP
            if ($scope.searchActive) {
                var matchScore = parseInt(calculatedValue*100);
                if (matchScore < 1 ){ 
                    d3.select("[id='ttRead" + cor + "']").text(matchScore);
                    d3.select("[id='ttUnits" + cor + "']").text(""); 
                } else {
                    d3.select("[id='ttRead" + cor + "']").text(matchScore);
                    d3.select("[id='ttUnits" + cor + "']").text("% match"); 
                }
            } else {
                sensorToolTip(activeSensorRange, calculatedValue, cor);
            }
            var circ = d3.select("g#circ");
                cid = "[id='c" + cor + "']";
            var it = circ.select(cid).classed("offTip", true);
        }
        
        //CLEAR COUNTABLE EVENTS
            var countLayer = d3.select("#counts");
            countLayer.selectAll(".count").remove();
        
        //Replaces default values with correct values where applicable
        if (!$scope.searchActive) {
            for (var j = 0; j < calibratedResults.length; j++) {
                var cor = calibratedResults[j]["coreid"];
                read[j] = calibratedResults[j][activeSensorRange['name']];
                //IF THIS IS LIGHT READING IS GOOD
                if (read[j] != 10000){ 
                //OVERWRITE AVERAGE
                sensorToolTip(activeSensorRange, read[j], cor);
                var circ = d3.select("g#circ");
                    cid = "[id='c" + cor + "']";
                var it = circ.select(cid).classed("offTip", false);
                //DRAW COUNTABLE EVENTS
                if (activeSensorRange['interfaceName'] == "sound"){
                    var cx,cy;
                    d3.select("svg")
                    it.each( function(d){
                        cx = d3.select(this).attr("cx");
                        cy = d3.select(this).attr("cy");
                    });
                    var countPlace = countLayer.append("g").attr("class", "count");
                    for (var i=1;i<calibratedResults[j]["Noises"];i+=1){
                        countPlace.append("circle").style("stroke","deepskyblue")
                        .attr("cx",cx).attr("cy",cy).attr("r",6+3*i);
                    }
                }
                 if (activeSensorRange['interfaceName'] =="motion"){
                    var cx,cy;
                    d3.select("svg")
                    it.each( function(d){
                        cx = d3.select(this).attr("cx");
                        cy = d3.select(this).attr("cy");
                    });
                    var countPlace = countLayer.append("g").attr("class", "count");
                    for (var i=1;i<calibratedResults[j]["Motions"];i+=1){
            //countPlace.append("circle").style("stroke","HotPink").attr("cx",cx).attr("cy",cy).attr("r",6+3*i);
                    }
                }
                    
                it.style("fill", function(d) { 
                    var col = featureGradient.getColor([parseInt(read[j])]);
                    col = col[0].cssColor;
                    return col;
                }) 
            }
            }
        }  // redraws all the colors and tooltips 
        histogram(read, featureGradient);
        $scope.hideSpinner();
        //console.log('turn off spinner');
        
    }
    // LOAD ORDER ACTION
    // Here's the main run-loops for the map 
    // Set up the drawing on SVG load, and then draw with sensor data
    $interval(function () {
        if ($scope.historical == null) {//if not historical...
            $scope.showSpinner();
            ParseFactory.getLive().then(function(newHot){ 
                calibratedResults = calibrateReadings(newHot);
                $scope.currentResults = calibratedResults;
                calculateDistances();
                drawColorGrid();  
            //    console.log("reloaded data");
            });
        }
    }, pollingInterval);

    $scope.svgLoaded = function () { // on initial SVG load
        loadNetwork();
        calculateDistances();
        drawColorGrid();
        
    } // runs once the svg has been loaded via ng-include

    $scope.$on("$stateChangeSuccess", function () {
        $scope.isSearchActive();
        drawColorGrid();
    }); // redraw data on tab click
});
