// angular.module is a global place for creating, registering and retrieving Angular modules

var app = angular.module('serg', ['ionic', 'serg.controllers', 'serg.services', 'chart.js'
                                  //, 'highcharts-ng'
                                ]);

// the following setting is to toggle between web dev and live device builds
// this toggle will bypass whatever device code will otherwise bork the web preview
mobileOn = false;

app.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    $ionicConfigProvider.tabs.position("top");

// Ionic uses AngularUI Router which uses the concept of states
// Set up the various states which the app can be in.
 // Each state's controller can be found in controllers.js
    $stateProvider
      // Set up login states
        .state('login', {
            url: '/login',
            templateUrl: 'templates/login.html',
            controller: 'LoginCtrl'
        })
        .state('onboard', {
            url: '/onboard',
            templateUrl: 'templates/onboard.html',
            controller: 'LoginCtrl'
        })
        .state('settings', {
          url: '/settings',
          templateUrl: 'templates/settings.html',
          controller: 'SettingsCtrl'
      })
    
    // setup an abstract state for the tabs directive
        .state('tab', {
            url: "/tab",
            abstract: true,
            templateUrl: "templates/tabs.html"
        })
    // Each tab has its own nav history stack:
  
        //.state('tab.dash', {
        //    url: '/dash',
        //    views: {
        //        'tab-dash': {
        //            templateUrl: 'templates/tab-dash.html',
        //            controller: 'DashCtrl'
        //        }
        //    }
        //})

        .state('tab.map', {
          url: '/map/{sensorId}',
          // Use `resolve` to resolve any asynchronous controller dependencies (in this case: a factory)
          // *before* the controller is instantiated. In this case, waits til the Parse results are returned to instatiate the view
          resolve: {
              initialResults: function (ParseFactory) {
                  return ParseFactory.getLive();
              },
              long: function (ParseFactory) {
                  //return ParseFactory.LiveHistory();
              }
          },
          views: {
              'tab-map': {
                  templateUrl: 'templates/tab-map.html',
                  controller: 'MapCtrl'
              }
          }
  });

//.state('tab.chats', {
//      url: '/chats',
//      views: {
//        'tab-chats': {
//          templateUrl: 'templates/tab-chats.html',
//          controller: 'ChatsCtrl'
//        }
//      }
//    })
  
//.state('tab.chat-detail', {
//      url: '/chats/:chatId',
//      views: {
//        'tab-chats': {
//          templateUrl: 'templates/chat-detail.html',
//          controller: 'ChatDetailCtrl'
//        }
//      }
//    });

    // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/login');

    // Center the title
  $ionicConfigProvider.navBar.alignTitle('center');

    // Stop the stupid Android top tabs
  //$ionicConfigProvider.tabs.position('bottom');
});

//// ArTiSTiX suggested workaround
//app.directive('range', function rangeDirective() {
//    return {
//        restrict: 'C',
//        link: function (scope, element, attr) {
//            element.bind('touchstart mousedown', function(event) {
//                event.stopPropagation();
//                event.stopImmediatePropagation();
//            });
//        }
//    };
//});


// Notice I am renaming the injected rootscope module to 'scope', 
// which means anything I put in the scope here will be available in the entire app.
// Probably not best practice
app.run(["$ionicPlatform", "$rootScope", "$state", "$ionicSlideBoxDelegate", "$timeout", "$ionicLoading", "$timeout", function ($ionicPlatform, $scope, $state, $ionicSlideBoxDelegate, $timeout, $ionicLoading, $timeout) {

    // For controlling visibility of elements in HTML
    $scope.mobileOn = mobileOn;

    $ionicPlatform.ready(function () {
        // Show Splashscreen
        //navigator.splashscreen.show();
        //$timeout(function () {
        //    navigator.splashscreen.hide()
        //}, 3000);

        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleLightContent();
        }
        if (mobileOn) {
            navigator.splashscreen.hide();
            cordova.getAppVersion.getVersionNumber(function (version) {
                $scope.appVersion = version;
            });
        }
    });

    //// Set up spinners

    //$scope.showSpinner = function () {
    //    $ionicLoading.show({
    //        template: '<ion-spinner></ion-spinner>'
    //    });
    //    console.log('showSpinner()');
    //};

    //$scope.hideSpinner = function () {
    //    $ionicLoading.hide();

    //    console.log('hideSpinner()');
    //};

    // Set default state and get current user, if exists

    $scope.scenario = 'Sign up';

    $scope.currentUser = Parse.User.current();

    var dimensions = {};

    // collect user data and report app open event
    if (mobileOn) {
        if ($scope.currentUser) {
            console.log($scope.currentUser.attributes.username, $scope.currentUser.attributes.devicePlatform, $scope.currentUser.attributes.deviceModel);

            dimensions = {
                username: $scope.currentUser.attributes.username != null ? $scope.currentUser.attributes.username : "unidentified",          
                platform: $scope.currentUser.attributes.devicePlatform != null ? $scope.currentUser.attributes.devicePlatform : "unidentified",
                device: $scope.currentUser.attributes.deviceModel != null ? $scope.currentUser.attributes.deviceModel : "unidentified"
            }        
        } else {
            dimensions = {
                username: "unidentified",          
                platform: "unidentified",
                device: "unidentified"
            }
        }


        // console.log(dimensions);
        Parse.Analytics.track('appOpen', dimensions);

    };

    // If there is a user logged in, go to map screen
    if ($scope.currentUser) {
        if ($scope.currentUser.attributes.emailVerified) {
            console.log('found verified user: ', $scope.currentUser);
            // fire camera here, make sure cordova is ready first.
            // alert('found user');
            if (mobileOn) {
                $ionicPlatform.ready(function () {
                    // Let's get this party started.
                    $timeout(function () { $state.go("tab.map");; }, 15);
                });
            } else {

            }


        } else {
            console.log('found unverified user');
            $scope.scenario = "Log in";
        }

    }

    $scope.signUp = function (form) {
        console.log(form);

        // prevent signup if email doesn't pass validation
        var x = form.email;
        if (form.email != undefined) {
            var atpos = x.indexOf("@");
            var dotpos = x.lastIndexOf(".");
        }
        if (form.email == undefined || atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length) {
            if (mobileOn) {
                window.plugins.toast.show("Please enter a valid e-mail address.", 'short', 'center', function (a) { console.log('toast success: ' + a) }, function (b) { console.log('toast error: ' + b) });
            } else {
                alert("Please enter a valid e-mail address.");
            }
            return;
        }

        var user = new Parse.User();
        user.set("email", form.email);
        user.set("username", form.username);
        user.set("password", form.password);

        // var promise = cordovaReady.ready();
        //   promise.then(function() {
        if (mobileOn) {
            user.set('deviceModel', device.model);
            user.set('deviceCordova', device.cordova);
            user.set('devicePlatform', device.platform);
            user.set('deviceUuid', device.uuid);
            user.set('deviceVersion', device.version);

            user.set('deviceModels', [device.model]);
            user.set('deviceCordovas', [device.cordova]);
            user.set('devicePlatforms', [device.platform]);
            user.set('deviceUuids', [device.uuid]);
            user.set('deviceVersions', [device.version]);
        }
        // });



        console.log(user);

        user.signUp(null, {
            success: function (user) {

                $scope.onboarding = true;
                $state.go("onboard");
                $scope.currentUser = user;
                console.log("signed up: ", $scope.currentUser);

                // set up signup tracking attributes
                if (mobileOn) {
                    var dimensions = {
                        username: user.attributes.username,
                        platform: user.attributes.devicePlatform,
                        model: user.attributes.deviceModel,
                    }

                    Parse.Analytics.track('signup', dimensions);
                }

                

            },
            error: function (user, error) {
                if (mobileOn) {
                    window.plugins.toast.show("Hmmmmmm.... there's a problem:  " + error.code + " " + error.message, 'long', 'center', function (a) { console.log('toast success: ' + a) }, function (b) { console.log('toast error: ' + b) });

                } else {
                    alert("Unable to sign up:  " + error.code + " " + error.message);

                }
                var dimensions = {
                    platform: device.platform,
                    device: device.model,
                    UUID: device.uuid,
                    activity: "signup"
                }

                // console.log(dimensions);
                if (mobileOn) {
                    Parse.Analytics.track('errorMsg', error.code + ": " + error.message);
                }
            }
        });
    };

    $scope.logIn = function (form) {
        Parse.User.logIn(form.username, form.password, {
            success: function (user) {
                if (user.attributes.emailVerified) {
                    
                    $state.go("tab.map");
                    // <a href="" data-role="button" class="ui-btn-right" ngm-vclick="sendFeedback()"><i class="fa fa-comments"></i></a>("/#onboard1");  // for debugging onbaording without creating a new user.

                } else {

                    //$state.go("tab.map");
                    $state.go('login');
                    if (mobileOn) {
                        window.plugins.toast.show('Please check your digital talkbox to verify your account.', 'long', 'center', function (a) { console.log('toast success: ' + a) }, function (b) { console.log('toast error: ' + b) });

                    } else {
                        alert('Please verify your email and log in again.');

                    }

                }
                $scope.currentUser = user;
                console.log("loggedIn: ", $scope.currentUser);

                dimensions = {
                    username: $scope.currentUser.attributes.username != null ? $scope.currentUser.attributes.username : "unidentified",
                    platform: $scope.currentUser.attributes.devicePlatform != null ? $scope.currentUser.attributes.devicePlatform : "unidentified",
                    device: $scope.currentUser.attributes.deviceModel != null ? $scope.currentUser.attributes.deviceModel : "unidentified",
                    emailVerified: user.attributes.emailVerified
                }

                if (mobileOn) {
                    Parse.Analytics.track('login', dimensions);
                }

            },
            error: function (user, error) {
                // alert("Unable to log in: " + error.code + " " + error.message);
                if (mobileOn) {
                    window.plugins.toast.show("There seems to be a problem:  " + error.code + " " + error.message, 'long', 'center', function (a) { console.log('toast success: ' + a) }, function (b) { console.log('toast error: ' + b) });

                    var dimensions = {
                        platform: device.platform,
                        device: device.model,
                        UUID: device.uuid,
                        activity: "login"
                    }

                    // console.log(dimensions);
                    Parse.Analytics.track('errorMsg', error.code + ": " + error.message);

                } else {
                    alert("Unable to log in:  " + error.code + " " + error.message);
                }

            }
        });
    };

    $scope.resetPassword = function () {
        var email = prompt("No problem, what's your digital talkbox address?");
        Parse.User.requestPasswordReset(email, {
            success: function () {
                // Password reset request was sent successfully
                if (mobileOn) {
                    window.plugins.toast.show("Check your email!", 'short', 'center', function (a) { console.log('toast success: ' + a) }, function (b) { console.log('toast error: ' + b) });
                } else {
                    alert("Check your email!");
                }

            },
            error: function (error) {
                // Show the error message somewhere
                alert("Error: " + error.code + " " + error.message);
            }
        });
    };

    //$scope.logOut = function (form) {
    //    Parse.User.logOut();
    //    $scope.currentUser = null;

    //    $scope.scenario = 'Log in';
    //    $state.go("login");
    //};

    $scope.gotoOnboarding = function () {
        // Return to Slide 0
        //$ionicSlideBoxDelegate.slide(0);
        $timeout(function () {
            $ionicSlideBoxDelegate.$getByHandle('onboardingBox').slide(0,[3]);
            $scope.$apply();
        }, 30);

        // Reset onbaording states and go to template
        $scope.onboarding = true;
        $state.go("onboard");

        
    }
    $scope.gotoMap = function () {
        $state.go("tab.map");
    }

    

}]);

(function () {"use strict";
    angular
        .module('app.Directives.range', [])
        .directive('range', rangeDirective);

    function rangeDirective() {
        return {
            restrict: 'C',
            link: function (scope, element, attr) {
                element.bind('touchstart mousedown', function (event) {
                    event.stopPropagation();
                    event.stopImmediatePropagation();
                });
            }
        };
    }
})();
